/**
 * Created by ranxianghui on 2017/9/13.
 */
export class CustomMenu {
    constructor(
        public id: number,
        text: string,
        heading?: boolean,
        link?: string,     // internal route links
        elink?: string,    // used only for external links
        target?: string,   // anchor target="_blank|_self|_parent|_top|framename"
        icon?: string,
        alert?: string,
        submenu?:Array<CustomMenu>
    ) { }
}