import { Component, OnInit } from '@angular/core';
import {MenuService} from "../core/menu/menu.service";

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    isShowMenu: boolean;
    constructor(public menu: MenuService) {
        this.isShowMenu = menu.getIsShowMenu();

    }

    ngOnInit() {
        alert("LayoutComponent")
    }

}
