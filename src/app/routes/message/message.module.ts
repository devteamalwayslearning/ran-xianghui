import { NgModule } from '@angular/core';
import { MessageComponent } from './message/message.component';
import { Routes, RouterModule } from '@angular/router';
import {DropdownComponent} from './message/dropdown/dropdown.component'

const routes: Routes = [
    { path: '', component: MessageComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: [MessageComponent,DropdownComponent],
    exports: [
        RouterModule
    ]
})
export class MessageModule { }