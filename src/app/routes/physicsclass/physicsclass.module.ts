import { NgModule } from '@angular/core';
import { PhysicsClassComponent } from './physicsclass/physicsclass.cpmponent';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layout/layout.module';

import { SharedModule } from '../../shared/shared.module';


const routes: Routes = [
    { path: '', component: PhysicsClassComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        LayoutModule
    ],
    declarations: [PhysicsClassComponent],
    exports: [
        RouterModule
    ]
})
export class PhysicsClassModule { }