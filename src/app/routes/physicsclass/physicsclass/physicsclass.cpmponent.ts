import { Component, OnInit } from '@angular/core';
import {MenuService} from "../../../core/menu/menu.service";


@Component({
    selector: 'app-physics-class',
    templateUrl: 'physicsclass.component.html',
    styleUrls: ['physicsclass.component.scss']
})
export class PhysicsClassComponent implements OnInit {



    mymenu:any;
    constructor( public menu : MenuService) {
        this.mymenu = menu;
    }

    ngOnInit() {
        this.mymenu.setIsShowMenu(false);
        alert("PhysicsClassComponent")
    }

    // ngOnDestroy(): void {
    //     this.mymenu.setIsShowMenu(true)
    //     throw new Error('Method not implemented.');
    // }
}
