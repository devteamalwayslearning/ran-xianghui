/**
 * Created by ranxianghui on 2017/9/15.
 */

import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-physicsclasshome',
    templateUrl: 'physicsclasshome.component.html',
    styleUrls: ['physicsclasshome.component.scss']
})
export class PhysicsClassHomeComponent implements OnInit {
    physicsClassName:string;
    classdatas:Array<any>;
    constructor() {
        this.physicsClassName = '物理班 GEDU-2389800';
        this.classdatas = this.crateDemoData();
    }

    ngOnInit() {
    }

    crateDemoData(){
        return [{
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        }, {
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            name:"张叶婷",
            status: '在读',
            physicsName:"XXXXXXX",
            physicsnumber: '1231312',
            useringclass: '10/-', //red
            studenttype: '资源优质型', //red
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },
        ]
    }
}
