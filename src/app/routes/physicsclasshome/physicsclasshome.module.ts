/**
 * Created by ranxianghui on 2017/9/15.
 */
import { NgModule } from '@angular/core';
import { PhysicsClassHomeComponent } from './component/physicsclasshome.component';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layout/layout.module';
import { SharedModule } from '../../shared/shared.module';


const routes: Routes = [
    { path: '', component: PhysicsClassHomeComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        LayoutModule
    ],
    declarations: [PhysicsClassHomeComponent],
    exports: [
        RouterModule,

    ]
})
export class PhysicsClassHomeModule { }