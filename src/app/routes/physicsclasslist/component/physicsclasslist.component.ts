/**
 * Created by ranxianghui on 2017/9/14.
 */
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-physics-classlist',
    templateUrl: 'physicsclasslist.component.html',
    styleUrls: ['physicsclasslist.component.scss']
})
export class PhysicsClassListComponent implements OnInit {

    classrooms: Array<any> ;
    classState:Array<any>;
    constructor( ) {
       this.classrooms = this.crateDemoData()
       this.classState = ["未结班","在读","待开班","结课","延期"]
    }

    ngOnInit() {
    }
    crateDemoData(){
        return [{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'

        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            pmockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },{
            physicsnumber: 'GEDU-081190',
            status: '在读',
            classnumber: '08501', //red
            time: '2017.06.28-2017.10.22', //red
            studentnumber:'10',
            progress:'10/24',
            teaching :'80',
            coaching:'10',
            operating:'20',
            mockexam:'5',
            returnvisit:'2'
        },]
    }

}
