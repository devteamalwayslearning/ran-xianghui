/**
 * Created by ranxianghui on 2017/9/14.
 */
import { NgModule } from '@angular/core';
import { PhysicsClassListComponent } from './component/physicsclasslist.component';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import {Ng2TableModule} from'ng2-table/ng2-table';
import {DataTableModule} from 'angular2-datatable';
const routes: Routes = [
    { path: '', component: PhysicsClassListComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        Ng2TableModule,
        DataTableModule
    ],
    declarations: [PhysicsClassListComponent],
    exports: [
        RouterModule
    ]
})
export class PhysicsClassListModule { }