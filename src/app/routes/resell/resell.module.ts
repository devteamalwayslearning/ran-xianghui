import { NgModule } from '@angular/core';
import { ResellComponent } from './resell/resell.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {BsDropdownModule}from'ngx-bootstrap'



const routes: Routes = [
    { path: '', component: ResellComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        BsDropdownModule,
    ],
    declarations: [ResellComponent],
    exports: [
        RouterModule
    ]
})
export class ResellModule { }