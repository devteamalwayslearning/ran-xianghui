import { Component, OnInit } from '@angular/core';

@Component({

    selector: 'app-schedule',
    templateUrl: 'resell.component.html',
    styleUrls: ['resell.component.scss']
})
export class ResellComponent implements OnInit {
    reselldatas: Array<any> = this.crateDemoData();
    constructor() { }

    ngOnInit() {
    }
    crateDemoData(){
        return [{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },{
            name: '张一',
            className: 'GEDU-081190',
            studentNumber: '3人班', //red
            progress: '10/24', //red
            startDate:'1027.06.28',
            endDate:'1027.10.22',
            studentType:'成绩取得型',
            backVisit1:'1/1',
            backVisit2:'1/1',
            backVisit3:'1/1',
            examVisit:'1/1',
            otherVisit:'1',
            saName:'王一'
        },]
    }
}
