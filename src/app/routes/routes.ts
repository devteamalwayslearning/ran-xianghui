import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';;
export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'schedule', loadChildren: './schedule/schedule.module#ScheduleModule' },
            { path: 'student', loadChildren: './student/student.module#StudentModule' },
            { path: 'message', loadChildren: './message/message.module#MessageModule' },
            { path: 'resell', loadChildren: './resell/resell.module#ResellModule' },
            { path: 'physicsclass', loadChildren: './physicsclass/physicsclass.module#PhysicsClassModule' },
            { path: 'physicsclasslist', loadChildren: './physicsclasslist/physicsclasslist.module#PhysicsClassListModule' },
            { path: 'physicsclasshome', loadChildren: './physicsclasshome/physicsclasshome.module#PhysicsClassHomeModule' },

        ]
    },
    // Not lazy-loaded routes
    //pages
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component ,data:{}},
    { path: '**', redirectTo: 'home' }
];

/* ---------------------------------------
    物理班路由
----------------------------------------- */
// export const physics_class_routes = [
//
//     {
//         path: 'physicsclass',
//         component: LayoutComponent,
//         children: [
//             { path: 'home', loadChildren: './home/home.module#HomeModule' },
//             { path: 'schedule', loadChildren: './schedule/schedule.module#ScheduleModule' },
//             { path: 'student', loadChildren: './student/student.module#StudentModule' },
//             { path: 'message', loadChildren: './message/message.module#MessageModule' },
//             { path: 'resell', loadChildren: './resell/resell.module#ResellModule' },
//         ]
//     },
//     { path: '**', redirectTo: 'home' }
// ];
