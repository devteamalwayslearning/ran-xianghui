import { NgModule } from '@angular/core';
import { ScheduleComponent } from './schedule/schedule.component';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule }from '../../layout/layout.module';

const routes: Routes = [
    { path: '', component: ScheduleComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        LayoutModule
    ],
    declarations: [ScheduleComponent ],
    exports: [
        RouterModule,

    ]
})
export class ScheduleModule { }