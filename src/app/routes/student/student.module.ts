import { NgModule } from '@angular/core';
import { StudentComponent } from './student/student.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', component: StudentComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: [StudentComponent],
    exports: [
        RouterModule
    ]
})
export class StudentModule { }